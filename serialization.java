/* A program to demonstrate serialization for various different dependencies */


import java.io.*; 
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

class Demo extends Area  //implements Serializable 
{  
  int a; 
  String b; 
 def D; 
 
 public Demo(String place, int zipcode, int a, String b,int id,String name) 
 { 
     super(place,zipcode);
     D = new def(id,name);
     this.a = a; 
     this.b = b; 
 } 
} 

class Test1 
{ 
 public static void main(String[] args) 
 {    
     Demo object = new Demo("vizag",530032,1, "abc",12,"abc"); 
     String filename = "file.txt"; 
       
      
     try
     {    
         
         FileOutputStream file = new FileOutputStream(filename); 
         ObjectOutputStream out = new ObjectOutputStream(file); 
           
         
         out.writeObject(object); 
           
         out.close(); 
         file.close(); 
           
         System.out.println("Object has been serialized"); 

     } 
       
     catch(IOException ex) 
     { 
         System.out.println("IOException is caught"+ex); 
     } 

     try
     {    
         
         FileInputStream file = new FileInputStream(filename); 
         ObjectInputStream in = new ObjectInputStream(file); 
           
         
         object =(Demo)in.readObject(); 
	

           
         in.close(); 
         file.close(); 
           
         System.out.println("Object has been deserialized "); 
         System.out.println("a = " + object.a); 
         System.out.println("b = " + object.b); 
	System.out.println("b = " + object.place); 
	System.out.println("b = " + object.zipcode); 
	System.out.println("b = " + object.D.name); 
	System.out.println("b = " + object.D.id); 
     } 
       
     catch(IOException ex) 
     { 
         System.out.println("IOException is caught"+ex); 
     } 
       
     catch(ClassNotFoundException ex) 
     { 
         System.out.println("ClassNotFoundException is caught"); 
     } 

 } 
}